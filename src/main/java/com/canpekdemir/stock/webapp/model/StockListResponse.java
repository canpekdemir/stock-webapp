package com.canpekdemir.stock.webapp.model;

import java.util.List;

public class StockListResponse {

    private String status;
    private List<Stock> stocks;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(List<Stock> stocks) {
        this.stocks = stocks;
    }
}
