package com.canpekdemir.stock.webapp.service;

import com.canpekdemir.stock.webapp.model.Stock;
import com.canpekdemir.stock.webapp.model.StockListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;

@Service
public class StockService {

    @Autowired
    RestTemplate restTemplate;

    public List<Stock> retrieveStocks() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8080" + "/api/v1/stocks");

        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<StockListResponse> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity<>(headers), StockListResponse.class);

        StockListResponse responseBody = response.getBody();

        return responseBody.getStocks();
    }
}
