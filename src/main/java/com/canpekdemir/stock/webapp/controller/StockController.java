package com.canpekdemir.stock.webapp.controller;

import com.canpekdemir.stock.webapp.model.Stock;
import com.canpekdemir.stock.webapp.service.StockService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class StockController {

    private StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @RequestMapping(value = "/stocks", method = RequestMethod.GET)
    public String retrieveStockList(ModelMap modelMap) {
        List<Stock> stocks = stockService.retrieveStocks();
        modelMap.addAttribute("stocks", stocks);
        return "stockList";
    }

}
